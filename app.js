var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var neo4jv = require('neo4j-js');

var index = require('./routes/index');
var app = express();
var Util = require('./models/util/Util');
var constants = require('./constants');
var graph = null;
const exmptedRoutes = ['/auth/signup', '/auth/login'];
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


neo4jv.connect(constants.NEO4J_URL, function (err, graph1) {
  if (err) {
    console.error('Could not connect to data base, exiting... ');
    process.exit();
  } else {
    graph = graph1;
    app.graph = graph;
    console.log('successfully connected to data base')
  }
});


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(checkAuthorization);
app.use('/', index);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function checkAuthorization(req, res, next) {

  if (exmptedRoutes.indexOf(req.originalUrl) > -1) {
    return next();
  }

let decoded = Util.isRequestHasValidToken(req);
  if (decoded) {
    app.decoded = decoded;
    next();
  } else {
    res.redirect(constants.LOGIN_URL);
  }
}
module.exports = app;
