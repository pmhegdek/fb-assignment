var express = require('express');
var router = express.Router();
let Data = require('./../models/Data');
let constants = require('./../constants');
/* GET users listing. */
router.get('/', function (req, res, next) {
  let data = new Data();
  let skip = req.query['start'] ? req.query['start'] : 0;
  let limit = req.query['length'] ? req.query['length'] : 10;
  data.getData(req.app.decoded['role'], skip, limit, req.app.graph)
    .then(data => {
      res.status(200).json({
        'data': data
      })
    }).catch(error => {
      console.log(error);
      res.status(500).json({
        'message': 'Internal server Error'
      })
    })
});

module.exports = router;
