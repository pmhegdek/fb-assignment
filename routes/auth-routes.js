let express = require('express')
let router = express.Router()
let User = require('./../models/User');
let Token = require('./../models/Token');
let Util = require('./../models/util/Util');

const constants = require('./../constants');

// this api renders login page
router.get('/login', function (req, res) {
    if (Util.isRequestHasValidToken(req)) {
        res.redirect(constants.APP_URL);
    } else {
        res.render('login', { 'message': '' });
    }
})

// this api handles login request
router.post('/login', function (req, res) {
    let user = new User(req.body['name'], req.body['email'].toLowerCase(), req.body['password']);
    user.isUserEmailAndPasswordMatches(req.app.graph)
        .then(result => {
            if (result.length > 0) {
                let token = Token.create({ 
                    data: user.email,
                    role: result[0]['role']
                 });
                res.cookie(constants.COOKIE_NAME, token, {
                    maxAge: 60 * 60 * 24 // 24 hours
                });
                res.redirect(constants.APP_URL);
            } else {
                res.render('login', { 'message': 'Invalid email id and password pair.' })
            }
        }).catch(error => {
            console.log(err)
            res.status(500).json({
                message: "Internal server error"
            });
        });
})

// this api renders signup page
router.get('/signup', function (req, res) {
    if (Util.isRequestHasValidToken(req)) {
        res.redirect(constants.APP_URL);
    } else {
        res.render('signup', {});
    }
})

// this api handles signup request
router.post('/signup', function (req, res) {
    let role = req.body.role ? req.body.role : constants.ROLE.BASIC;
    let user = new User(req.body['name'], req.body['email'].toLowerCase(), req.body['password'], role);
    user.isUserEmailExistsInDB(req.app.graph)
        .then(data => {
            if (data.length > 0) {
                res.render('login', { 'message': 'Email ID is already registered. Please login.' })
            } else {
                return user.saveToDB(req.app.graph);
            }
        })
        .then(data => {
            let token = Token.create({ 
                data: user.email ,
                role: role
            });
            res.cookie(constants.COOKIE_NAME, token, {
                maxAge: 60 * 60 * 24 // 24 hours
            });
            res.redirect(constants.APP_URL);
        })
        .catch(err => {
            console.log("error:  " + err);
            res.status(500).json({
                message: "Internal server error"
            });
        });
})

router.get('/logout', function (req, res) {
    res.clearCookie(constants.COOKIE_NAME);
    res.redirect(constants.LOGIN_URL);
})


module.exports = router;