var express = require('express');
var router = express.Router();
var data = require('./data-routes.js');
let authRouter = require('./auth-routes.js');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'City-Inspection', header:'City-Inspection' });
});

router.use('/data', data);
router.use('/auth',authRouter);
module.exports = router;
