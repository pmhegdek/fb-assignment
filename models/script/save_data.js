const constants = require('./../../constants')
var neo4jv = require('neo4j-js');
let Data = require('./../Data');

var graph;

let data = require('./../../city_inspections');

neo4jv.connect(constants.NEO4J_URL, function (err, graph1) {
    if (err) {
        console.log('error connecting to neo4j');
    } else {
        console.log('successfully connected to neo4j');
        graph = graph1;
        for(let i = 1800; i < 2800; i++){
        loadNextData(i);
        }
    }
});
function loadNextData(i){
    let d = data[i];
    let dObj = new Data(d['_id']['$oid'], d['id'], d['certificate_number'], d['business_name'], d['date'], d['result'], d['sector'], d['address']['city'],d['address']['zip'],d['address']['street'],d['address']['number']);

                dObj.isIDExists(graph)
                    .then( res =>{
                        if(res.length > 0){

                        }else{
                            dObj.saveToDB(graph)
                            .then( result =>{
                                console.log('success: ' + result );
                            }).catch( error => {
                                console.log('error: ' + error);
                            });
                        }
                    }).catch(error=>{
                        console.log(error)
                    })
            }
