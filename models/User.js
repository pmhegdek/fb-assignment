const constants = require('./../constants')

class User {
    constructor(name, email, password, role) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    isUserEmailExistsInDB( graph) {
        return new Promise(function (resolve, reject) {
            let query = "MATCH (user:fUser)" +
                "WHERE user.email =\"" + this.email + "\" " +
                "RETURN user.email as email, user.role as role "
            graph.query(query, function (err, result) {
                if (err) reject(err);

                resolve(result);
            })
        }.bind(this));
    }

    isUserEmailAndPasswordMatches( graph) {
        return new Promise(function (resolve, reject) {
            let query = "MATCH (user:fUser) " +
                "WHERE user.email =\"" + this.email + "\" AND user.password =\"" + this.password + "\" " +
                "RETURN user.email as email, user.role as role "
                console.log('isUserEmailAndPasswordMatches query: ' + query);
            graph.query(query, function (err, result) {
                if (err) reject(err);
                resolve(result);
            })
        }.bind(this));
    }

    saveToDB(graph) {
        return new Promise(function (resolve, reject) {
            let query = "CREATE (user:fUser{"
                + "name:\"" + this.name + "\", "
                + "password:\"" + this.password + "\", "
                + "role:\"" + this.role + "\", "
                + "email:\"" + this.email + "\" "
                + " }) RETURN ID(user) AS id";

            graph.query(query, function (err, result) {
                if (err) reject(err);
                resolve(result);
            })
        }.bind(this));
    }
}

module.exports = User;