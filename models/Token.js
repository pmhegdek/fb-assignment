let jwt = require('jsonwebtoken');
let constants = require('./../constants');

class Token {
    constructor(token) {
        this.token = token;
    }

    isValid() {
        return jwt.verify(this.token, constants.JWT_TOKEN_SECRET);
    }

    static create(payload) {
        let token = jwt.sign(payload, constants.JWT_TOKEN_SECRET, {
            expiresIn: 60 * 60 * 24 * 1 // 1 day
        });
        return token;
    }
}

module.exports = Token;