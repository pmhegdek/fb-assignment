let Token = require('./../Token');
let constants = require('./../../constants');

class Util {
    constructor() { }

    static isRequestHasValidToken(req) {
        let signedToken = req.body.token || req.query.token || req.headers['x-access-token'] || req.cookies[constants.COOKIE_NAME];
        if (signedToken) {
            let tokenObj = new Token(signedToken);
            return tokenObj.isValid();
        }
    }

    static getCurrentTime() {
        let date = new Date();
        let day = date.getDate();
        let month = (date.getMonth() + 1);
        let year = date.getFullYear();
        let hour = date.getHours();
        let min = date.getMinutes();
        let sec = date.getSeconds();
        let milliSec = date.getMilliseconds();
        let todaysDate = year * Math.pow(10, 13) + month * Math.pow(10, 11) + day * Math.pow(10, 9) + hour * Math.pow(10, 7) + min * Math.pow(10, 5) + sec * Math.pow(10, 3) + milliSec;
        return todaysDate;
    }

    static msToTime(duration) {
        var milliseconds = parseInt((duration % 1000) / 100)
            , seconds = parseInt((duration / 1000) % 60)
            , minutes = parseInt((duration / (1000 * 60)) % 60)
            , hours = parseInt((duration / (1000 * 60 * 60)) % 24);

        hours = (hours < 10) ? "0" + hours : hours;
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        seconds = (seconds < 10) ? "0" + seconds : seconds;
        return hours + ":" + minutes + ":" + seconds + "." + milliseconds;
    }

    static stringToTime(time) {
        let year = time.slice(0, 4);
        let month = time.slice(4, 2);
        let date = time.slice(6, 2);
        let hour = time.slice(8, 2);
        let minute = time.slice(10, 2);
        let seconds = time.slice(12, 2);
        return year + ' - ' + month + ' - ' + date + ' ' + hour + ':' + minute + ':' + seconds + ':'
    }

    static getResultFeilds(objname, fields) {
        let query = ''
        let multiple = false
        for (let i = 0; i < fields.length; i++) {
            if (multiple) {
                query += ', '
            }
            query += objname + '.' + fields[i] + ' as ' + fields[i]
            multiple = true
        }
        return query
    }
}

module.exports = Util;