const constants = require('./../constants')
let Util = require('./util/Util');

class Data {
    constructor(oid, id, certificate_number, business_name, date, result, sector, city, zip, street, number) {
        this.oid = oid;
        this.id = id;
        this.certificate_number = certificate_number;
        this.business_name = business_name;
        this.date = date;
        this.result = result;
        this.sector = sector
        this.address = {
            city: city,
            zip: zip,
            street: street,
            number: number
        }
        this.graph = null;
    }
    getDataFeildsForAdmin() {
        return ['id', 'oid', 'business_name', 'date', 'result', 'sector', 'certificate_number', 'street', 'city', 'number', 'zip'];
    }

    getDataFeildsForUser() {
        return ['id', 'oid', 'business_name', 'date', 'sector', 'street', 'city', 'number', 'zip'];
    }

    saveToDB(graph) {
        return new Promise(function (resolve, reject) {
            let query = "CREATE (data:fData{"
                + "oid:\"" + this.oid + "\", "
                + "id:\"" + this.id + "\", "
                + "certificate_number:\"" + this.certificate_number + "\", "
                + "business_name:\"" + this.business_name + "\", "
                + "date:\"" + this.date + "\", "
                + "result:\"" + this.result + "\", "
                + "sector:\"" + this.sector + "\", "
                + "certificate_number:\"" + this.certificate_number + "\", "
                + "city:\"" + this.address.city + "\", "
                + "zip:\"" + this.address.zip + "\", "
                + "street:\"" + this.address.street + "\", "
                + "number:\"" + this.address.number + "\" "
                + "}) RETURN ID(data) AS id";

            console.log('saveToDB query: ' + query);
            graph.query(query, function (err, result) {
                if (err) reject(err);
                resolve(result);
            })
        }.bind(this));
    }

    isIDExists(graph) {
        return new Promise(function (resolve, reject) {
            try {
                let query = "MATCH (data:fData)" +
                    "WHERE data.id =\"" + this.id + "\" " +
                    "RETURN data.id as id "
                graph.query(query, function (err, result) {
                    if (err) reject(err);
                    resolve(result);
                })
            } catch (error) {
                reject(error)
            }
        }.bind(this));
    }

    getData(role, skip, limit, graph) {
        return new Promise(function (resolve, reject) {
            try {
                let feilds = [];
                if (role === constants.ROLE.ADMIN) {
                    feilds = this.getDataFeildsForAdmin();
                } else {
                    feilds = this.getDataFeildsForUser();
                }

                let query = "MATCH (data:fData)" +
                    "RETURN  " + Util.getResultFeilds('data', feilds) +
                    " SKIP " + skip + " LIMIT " + limit;

                console.log('get data query: ' + query);

                graph.query(query, function (err, result) {
                    if (err) {
                        reject(err);
                    } else {
                        let newResult = [];
                        for (let i = 0; i < result.length; i++) {
                            let d = {
                                'Business_name': result[i]['business_name'],
                                'Date': result[i]['date'],
                                'Sector': result[i]['sector'],
                                'Address': result[i]['number'] + ', ' + result[i]['street'] + ', ' + result[i]['city'] + ', ' + result[i]['zip']
                            }
                            result[i]['certificate_number'] ? d['Certificate_number'] = result[i]['certificate_number'] : null;
                            result[i]['result'] ? d['Result'] = result[i]['result'] : null;
                            newResult.push(d);
                        }
                        resolve(newResult);
                    }
                });
            } catch (error) {
                console.log(error);
                reject(error)
            }
        }.bind(this));
    }
}

module.exports = Data;