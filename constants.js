module.exports = {
    NEO4J_URL: 'http://neo4j:pmhk1234$@13.127.140.161:7474/db/data/',
    JWT_TOKEN_SECRET: 'foodybuddy1234$',
    COOKIE_NAME: 'foodybuddy',
    SIGN_UP_URL: '/auth/signup',
    LOGIN_URL: '/auth/login',
    APP_URL: '/',
    ROLE: {
        ADMIN: 'admin',
        BASIC: 'basic'
    },
    PORT: '3000'
}